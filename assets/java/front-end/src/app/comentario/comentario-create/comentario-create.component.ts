import { Component, OnInit } from '@angular/core';
import {TokenStorageService} from '../../auth/token-storage.service';
import {Router} from '@angular/router';
import {Comentario} from '../comentario';
import {ComentarioService} from '../comentario.service';

@Component({
  selector: 'app-comentario-create',
  templateUrl: './comentario-create.component.html',
  styleUrls: ['./comentario-create.component.css']
})
export class ComentarioCreateComponent implements OnInit {
  comentario: Comentario = new Comentario();

  constructor(private token: TokenStorageService,
              private comentarioService: ComentarioService,
              private router: Router) { }

  ngOnInit() { }

  refresh(): void {
    window.location.reload();
  }

  onSubmit() {
    this.comentarioService.createComentario(this.comentario)
    .subscribe(data => console.log(data), error => console.log(error));
    this.comentario = new Comentario();
    this.refresh();

  }

  goToLogin() {
    this.router.navigate([('login')]);
  }

}
