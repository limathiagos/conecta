import { Component, OnInit } from '@angular/core';
import {Observable} from 'rxjs';
import {TokenStorageService} from '../../auth/token-storage.service';
import {Router} from '@angular/router';
import {Comentario} from '../comentario';
import {ComentarioService} from '../comentario.service';

@Component({
  selector: 'app-comentario-list',
  templateUrl: './comentario-list.component.html',
  styleUrls: ['./comentario-list.component.css']
})
export class ComentarioListComponent implements OnInit {

  key = 'nome';
  reverse = true;

  comentarios: Observable<Comentario[]>;
  constructor(private token: TokenStorageService,
              private comentarioService: ComentarioService,
              private router: Router) { }

  ngOnInit() {
    this.reloadData();
  }

  reloadData() {
    this.comentarios = this.comentarioService.getComentarios();
    console.log(this.comentarios);
  }


  editarComentario(id: number) {
    this.router.navigate(['editar-comentario', id]);
  }

  visualizarComentario(id: number) {
    this.router.navigate(['visualizar-comentario', id]);
  }

  deletarComentario(id: number) {
    this.comentarioService.deletarComentario(id)
    .subscribe(
      data => {
        console.log(data);
        this.reloadData();
      },
      error => console.log(error));
  }

  goToLogin() {
    this.router.navigate(['login']);
  }

  likeCount(): void {
    window.location.reload();
  }
}
