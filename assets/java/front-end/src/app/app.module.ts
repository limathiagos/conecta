import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { HomeComponent } from './home/home.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { httpInterceptorProviders } from './auth/auth-interceptor';
import { FooterComponent } from './shared/footer/footer.component';
import { HeaderComponent } from './shared/header/header.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { PostagemCreateComponent } from './postagem/postagem-create/postagem-create.component';
import { PostagemListComponent } from './postagem/postagem-list/postagem-list.component';
import { SidebarComponent } from './shared/sidebar/sidebar.component';
import {OrderModule} from 'ngx-order-pipe';
import {ComentarioCreateComponent} from './comentario/comentario-create/comentario-create.component';
import {ComentarioListComponent} from './comentario/comentario-list/comentario-list.component';
import { PostagemEditComponent } from './postagem/postagem-edit/postagem-edit.component';
import { PerfilDetailsComponent } from './perfil/perfil-details/perfil-details.component';




@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    HomeComponent,
    FooterComponent,
    HeaderComponent,
    PostagemCreateComponent,
    PostagemListComponent,
    SidebarComponent,
    ComentarioCreateComponent,
    ComentarioListComponent,
    PostagemEditComponent,
    PerfilDetailsComponent

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgbModule,
    OrderModule
  ],
  providers: [
    httpInterceptorProviders
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
export class YourAppModule {}
