/* tslint:disable */
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { TokenStorageService } from '../auth/token-storage.service';

@Injectable({
  providedIn: 'root'
})
export class PostagemService {

  private baseUrl = 'http://localhost:8080/api/v1/postagens';
  private header = {Authorization: `Bearer ${this.token.getToken()}`};

  constructor(private http: HttpClient, private token: TokenStorageService) { }

  getPostagem(id: number): Observable<any> {
    return this.http.get(`${this.baseUrl}/${id}`, {headers: this.header});
  }

  createPostagem(postagem: Object): Observable<Object> {
    return this.http.post(`${this.baseUrl}`, postagem, {headers: this.header});
  }

  updatePostagem(id: number, value: any): Observable<Object> {
    return this.http.put(`${this.baseUrl}/${id}`, value, {headers: this.header});
  }

  deletarPostagem(id: number): Observable<any> {
    return this.http.delete(`${this.baseUrl}/${id}`, {responseType: 'text', headers: this.header});
  }

  getPostagens(): Observable<any> {
    return this.http.get(`${this.baseUrl}`, {headers: this.header});
  }

}
