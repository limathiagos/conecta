import { Component, OnInit } from '@angular/core';
import {TokenStorageService} from '../../auth/token-storage.service';
import {Router} from '@angular/router';
import {Observable} from 'rxjs';
import {PostagemService} from '../postagem.service';
import {Postagem} from '../postagem';

@Component({
  selector: 'app-postagem-list',
  templateUrl: './postagem-list.component.html',
  styleUrls: ['./postagem-list.component.css']
})
export class PostagemListComponent implements OnInit {
  key = 'nome';
  reverse = true;
  like = 0;

  postagens: Observable<Postagem[]>;

  user: any;

  constructor(private token: TokenStorageService,
              private postagemService: PostagemService,
              private router: Router) { }

  ngOnInit() {
    this.reloadData();
  }

  reloadData() {
    this.postagens = this.postagemService.getPostagens();
    console.log(this.postagens);
  }


  editarPostagem(id: number) {
    this.router.navigate(['editar-postagem', id]);
  }

  visualizarPostagem(id: number) {
    this.router.navigate(['visualizar-postagem', id]);
  }

  deletarPostagem(id: number) {
    this.postagemService.deletarPostagem(id)
    .subscribe(
      data => {
        console.log(data);
        this.reloadData();
      },
      error => console.log(error));
  }

  goToLogin() {
    this.router.navigate(['login']);
  }

  likeCount(postagens: Postagem) {
    this.like += 1;
  }

}
