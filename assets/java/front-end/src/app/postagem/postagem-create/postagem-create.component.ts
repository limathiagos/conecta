import { Component, OnInit } from '@angular/core';
import {TokenStorageService} from '../../auth/token-storage.service';
import {Router} from '@angular/router';
import {PostagemService} from '../postagem.service';
import {Postagem} from '../postagem';

@Component({
  selector: 'app-postagem-create',
  templateUrl: './postagem-create.component.html',
  styleUrls: ['./postagem-create.component.css']
})
export class PostagemCreateComponent implements OnInit {

  postagem: Postagem = new Postagem();

  constructor(private token: TokenStorageService,
              private postagemService: PostagemService,
              private router: Router) { }

  ngOnInit() { }

  refresh(): void {
    window.location.reload();
  }

  onSubmit() {
    this.postagemService.createPostagem(this.postagem)
    .subscribe(data => console.log(data), error => console.log(error));
    this.postagem = new Postagem();
    this.refresh();

  }

  goToLogin() {
    this.router.navigate([('login')]);
  }
}
