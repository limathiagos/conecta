import {Comentario} from '../comentario/comentario';

export class Postagem {

  id: number;
  conteudoPostagem: string;
  user: string;
  comentario: Comentario[];
}
