import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-perfil-details',
  templateUrl: './perfil-details.component.html',
  styleUrls: ['./perfil-details.component.css']
})
export class PerfilDetailsComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }
  visualizarFeed() {
    this.router.navigate(['home']);
  }

}
